window.addEventListener("load", function() {
	// Stran naložena
		
	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
	
			// TODO:
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if(cas == 0) {
				var naziv = opomnik.querySelector(".naziv_opomnika").innerHTML;
				window.alert("Opomnik!\n\nZadolžitev "+naziv+" je potekla!");
				
				opomnik.remove();
			} else {
				casovnik.innerHTML = cas-1;
			}
		}
	};
	
	setInterval(posodobiOpomnike, 1000);
	
	document.getElementById("prijavniGumb").addEventListener('click', function(){
		var uporIme = document.querySelector("#uporabnisko_ime").value;
		
		document.getElementById("uporabnik").innerHTML = uporIme;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
		
		
	});
	
	document.getElementById("dodajGumb").addEventListener('click', dodajOpomnik);
	
	function dodajOpomnik() {
		var naz_op = document.getElementById("naziv_opomnika").value;
		var cas_op = document.getElementById("cas_opomnika").value;
		
		document.getElementById("naziv_opomnika").value = "";
		document.getElementById("cas_opomnika").value = "";
		
		document.getElementById("opomniki").innerHTML += 
		"<div class='opomnik rob senca'>"+
		     "<div class='naziv_opomnika'>"+naz_op+"</div>"+
		     "<div class='cas_opomnika'> Opomnik čez <span>"+cas_op+"</span> sekund.</div>"+
		"</div>";
		
	}
	
});